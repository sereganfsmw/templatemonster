<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Baners System</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="MoST">
<link rel="shortcut icon" href="favicon.png">

<!-- TemplateMonster CSS -->
<link href="css/template-monster.css" rel="stylesheet">

<!-- Стиои -->
<style>
  body {
    background-color: #f5f5f5;
    height: 100%;
  }
</style>

<!-- Скрипты -->
<script>

</script>

</head>

<div class="container">

<!-- Меню -->
<div class="menu-div">
  <ul class="menu">
    <li id="home" class="active"><a href="/">Главная</a></li>

    <? if ((!$this->user) and (!$this->admin)){ /*Если пользователь не авторизован*/?>
        <li id="log-in"><a href="/?login">Войти</a></li>
        <li id="reg"><a href="/?reg">Регистрация</a></li>
    <? } ?>

    <? if (($this->user) or ($this->admin)) {/*Если зашол пользователь или админ*/?>
        <li id="baners"><a href="/?baners">Банеры</a></li>
        <li id="add-post"><a href="/?add">Новый Банер</a></li>
        <li id="baners"><a href="/?images">Изображения</a></li>

        <li><a class="panel" href="#">Вы вошли как: <b><?= $_COOKIE['name'];?></b></a></li>
    
          <? if ($this->admin) { /*Если зашол администратор*/?>
              <li id="control-panel"><a href="/?control">Панель управления</a></li>
          <? } ?>

        <li><a href="/?logoff">Выход [x]</a></li>
    <? } ?>
  </ul>
</div>


  <body>
  <? //echo $_COOKIE['name'];//print_r ($this->user);//$_SESSION['user'] //print_r ($this->user);//print_r($_COOKIE); ?>
  <? $this->out($this->tpl,true); ?>

  </body>

    <!-- Footer -->
    <footer>
      Designed by <b><span class="most-text">Serega MoST</span></b>. Copyright © 2015 All Rights Reserved
    </footer>
</div>  <!-- /container -->


</html>


