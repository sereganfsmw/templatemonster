<?php
session_start(); //Начать сессию
//Роутер
//Author@: Serega MoST (02.02.2015)

class ctrlIndex extends ctrl {
    
    function index(){
        
        //$this->posts = $this->db->query("SELECT * FROM baners ORDER BY ctime DESC")->all();
        //echo "Hello Serega MoST";
        $this->out('home.php');
    }

    function baners(){
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");

        if ($this->user){
        //Показать банеры текущего пользователя
        $this->users = $this->db->query("SELECT * FROM users WHERE username = ?",$_COOKIE['username'])->all();
        $this->posts = $this->db->query("SELECT * FROM baners WHERE user = ? ORDER BY id ASC",$_COOKIE['username'])->all(); //Доделать для текущего пользователя
        }

        if ($this->admin){
        //Показать все банеры (Админ)
        $this->users = $this->db->query("SELECT * FROM admin WHERE login = ?",$_COOKIE['username'])->all();
        $this->posts = $this->db->query("SELECT * FROM baners ORDER BY id ASC")->all(); //Админ видит все записи
        }

        //Поиск банеров
        if(!empty($_POST)){
        $search='%'.$_POST['search'].'%';
        $this->posts = $this->db->query("SELECT * FROM baners WHERE title LIKE ?",$search)->all();
        }

        $this->out('baners.php');
    }
//________________________________________________________________________Get BANER ________________________________________________________________________
    function getBaners ($url,$user) {
        header('Content-Type: text/html');
        echo "<meta charset='UTF-8'>";
        echo "<link href='css/template-monster.css' rel='stylesheet'>";

        print "
        <style>
        // html{
        // overflow: hidden;
        </style>
        ";
        
        $curent_data = date("Y-m-d");
        //Если банер включен
        //Если банер есть в списке страниц для показа (*-для всех страниц)
        //Если дата оторажения банера есть в диапазоне между 'data_start' и 'data_end'
        $baner = $this->db->query("SELECT * FROM baners WHERE id IN (SELECT baner_id FROM pages WHERE page= ? ) AND ? BETWEEN data_start AND data_end AND user=? AND enable = ?",$url,$curent_data,$user,'true')->all();
        $count=0;
        
        if(!empty($baner))
        {
            $count++; //Количество вхождений
            shuffle($baner); 
            $max = sizeof($baner);
            $max = $max -1;
            $i = mt_rand(0, $max);
            $cur_banner = $baner[$i]; //Передаем текущий банер
            printf ("
                <script type='text/javascript'>
                    var json_banners = \"%s\";
                    var json_cur_banner = \"%s\";                   
                </script>
            ", preg_replace( "|\"|", "&quot;", json_encode($baner) ), preg_replace( "|\"|", "&quot;", json_encode($cur_banner) ) ); //Передаем масив банеров
            print "
            <a class='banner-link' target='_top' href='http://{$baner[$i]['url']}'>
            <div class='post-in-slide' <!--style='margin:0px;padding: 20px 10px 20px 10px;height:100%;-->'>{$cur_banner['baner']}</div>
            </a>
            ";
            printf (
                "<script type='text/javascript'>
                    document.querySelector('.post-in-slide').onmouseout = function() {
                        var banners = JSON.parse( json_banners.replace( /&quot;/g, \"\\\"\" ) ); //Парсим, заменяя слешы
                        var cur_banner = JSON.parse( json_cur_banner.replace( /&quot;/g, \"\\\"\" ) ); //Екранируем символы
                        console.log(cur_banner); //Выводим в консоль текущий елемент
                        var variants = banners.splice( 0, banners.length );
                        console.log(variants); //Выводим в консоль елементы что пришли
                        var variant;
                        var random_ind;
                        var new_banner;
                        var variant_indexes = []; //Варианты выборки
                        for ( var i=0; i<variants.length; i++ ){
                            variant = variants[i];
                            if ( variant['id'] != cur_banner['id'] ){ //Если индекс не равен текущнму то добавляем в масив для замены
                                variant_indexes.push( i );
                            }
                        }
                        random_ind = variant_indexes[ Math.floor( Math.random() * variant_indexes.length ) ]; //Рандомный индекс
                        new_banner = variants[random_ind]; //Новый банер который будет подставяться
                        this.innerHTML = new_banner.baner; //Вставить в текущий блок
                        json_cur_banner = JSON.stringify( new_banner ); //Преобразует значение JavaScript в строку JSON
                    }; 
                </script>"
            );
        }

        if ($count=='0'){ //Если не найдено банеров показать картинку TemplateMonster
            echo "<a href='http://{$_SERVER['SERVER_NAME']}'><img style='width: 100%; height: 100%;' src='tm.jpg'></a>";
        }
    }
//________________________________________________________________________Get BANER ________________________________________________________________________
    function getJs ($user) {
        header('Content-Type: text/javascript');
        print "
        //Возвращает cookie если есть или undefined
        function getCookie(name) {
            var matches = document.cookie.match(new RegExp(
              '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
            ))
            return matches ? decodeURIComponent(matches[1]) : undefined 
        }

        //Уcтанавливает cookie
        function setCookie(name, value, props) {
            props = props || {}
            var exp = props.expires
            if (typeof exp == 'number' && exp) {
                var d = new Date()
                d.setTime(d.getTime() + exp*1000)
                exp = props.expires = d
            }
            if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

            value = encodeURIComponent(value)
            var updatedCookie = name + '=' + value
            for(var propName in props){
                updatedCookie += '; ' + propName
                var propValue = props[propName]
                if(propValue !== true){ updatedCookie += '=' + propValue }
            }
            document.cookie = updatedCookie
        }

        //Удаляет cookie
        function deleteCookie(name) {
            setCookie(name, null, { expires: -1 })
        }

            document.addEventListener('DOMContentLoaded', function(event) { 
                     
                //Куки, посещения страниц пользователем
                if(!getCookie('views_count')) 
                {
                    views_count=1;
                    setCookie('views_count', views_count);
                }
                else
                {
                    views_count=getCookie('views_count');
                    views_count++;
                    setCookie('views_count', views_count)
                    //alert(views_count);
                }

                //Минимальное количество просмотров страниц, после которого показываем банер 5
                if (views_count>5) {

                    //Создание стилей CSS
                    var css = '#close-baner{display:block;    width:5px;    height:5px;    position:absolute;    top:1px;    right: 1px;    overflow:hidden;    text-align: center;    background:white;    z-index:90;    border: 1px white solid;    text-decoration: none;    cursor: pointer;    color: black;    -webkit-box-shadow:inset 0 2px 4px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05);-moz-box-shadow:inset 0 2px 4px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05);box-shadow:inset 0 2px 4px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05)}#close-baner:hover{    background: red;}',
                    head = document.head || document.getElementsByTagName('head')[0],
                    style = document.createElement('style');

                    style.type = 'text/css';
                    if (style.styleSheet){
                    style.styleSheet.cssText = css;
                    } else {
                    style.appendChild(document.createTextNode(css));
                    }

                    head.appendChild(style);

                    //Создание фрейма
                    var url = document.location.href.match(/[^\/]+$/)[0];
                    var parentElem = document.children[0];
                    var newDiv = document.createElement('div');
                    newDiv.id = 'baner';
                    newDiv.innerHTML = '<iframe id=\"frame\" style=\"border:1px solid #CCC; width:100%; height:100%; <!--display:none;-->\" src=\"http://tm-baners/?getBaners/'+url+'/$user\"></iframe><a id=\"close-baner\"></a>';
                    parentElem.appendChild(newDiv);

                    //Скрытие елементов при нажатии на квадратик
                    document.querySelector('#close-baner').onclick = function() {
                        var el = document.querySelector('#frame');
                        el.style.display = (el.style.display == 'none') ? '' : 'none'
                    }; 
                }
            });
        ";
    }
    function BanerState ($state,$postid) {
        //Не авторизированые пользователи не могут менять состояние банера
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("UPDATE baners SET enable = ? WHERE id = ?",$state,$postid);
    }

    function addBanerPages ($pages,$postid) {
        //Не авторизированые пользователи
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("INSERT INTO pages (page,baner_id) VALUES(?,?)",$pages,$postid);
    }

    function delBanerPages ($pages,$postid) {
        //Не авторизированые пользователи
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM pages WHERE page = ? AND baner_id = ?",$pages,$postid);
    }

    function BanerStartDate ($start,$postid) {
        //Не авторизированые пользователи
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("UPDATE baners SET data_start = ? WHERE id = ?",$start,$postid);
    }

    function BanerEndDate ($end,$postid) {
        //Не авторизированые пользователи
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("UPDATE baners SET data_end = ? WHERE id = ?",$end,$postid);
    }

    function control(){
    if (!$this->admin) return header ("Location: /");
    //HTTP Аутентификация Админа
    if 
        (
            !isset($_SERVER['PHP_AUTH_USER']) ||
            (
                ('admin' != $_SERVER['PHP_AUTH_USER']) ||
                ('admin' != $_SERVER['PHP_AUTH_PW'])
            )
        )
    {
        header('WWW-Authenticate: Basic relam="Admin Panel"');
        header('HTTP/1.0 401 Unauth');

        die();
        
    }
        $this->out('control.php');
    }

    function reg(){
        
        if(!empty($_POST)){

            $user = $this->db->query("SELECT * FROM users WHERE username = ?",$_POST['user'])->assoc();

            //Проверяем нету ли такого пользователя
            if ($user) {
                echo "<script>alert('Пользователь уже существует')</script>";
            } else {

            $user=$_POST['user'];
            $pass=$_POST['pass'];
            $name=$_POST['name'];
            $key=md5(date('YmdHis'));

            $this->db->query("INSERT INTO users (username,password,name,key_user) VALUES(?,?,?,?)",$user,$pass,$name,$key);
            echo "<script>alert('Пользователь зарегистрирован')</script>";
            //header("Location: /");

            }
        }
        $this->out('reg.php');
    }

    function images(){
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        if(!empty($_POST)){
            //Загрузка картинок
            function translit($string) //Функция Транслиста
            {
                $converter = array(
                    'а' => 'a',   'б' => 'b',   'в' => 'v',
                    'г' => 'g',   'д' => 'd',   'е' => 'e',
                    'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
                    'и' => 'i',   'й' => 'y',   'к' => 'k',
                    'л' => 'l',   'м' => 'm',   'н' => 'n',
                    'о' => 'o',   'п' => 'p',   'р' => 'r',
                    'с' => 's',   'т' => 't',   'у' => 'u',
                    'ф' => 'f',   'х' => 'h',   'ц' => 'c',
                    'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
                    'ь' => "'",   'ы' => 'y',   'ъ' => "'",
                    'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
             
                    'А' => 'A',   'Б' => 'B',   'В' => 'V',
                    'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
                    'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
                    'И' => 'I',   'Й' => 'Y',   'К' => 'K',
                    'Л' => 'L',   'М' => 'M',   'Н' => 'N',
                    'О' => 'O',   'П' => 'P',   'Р' => 'R',
                    'С' => 'S',   'Т' => 'T',   'У' => 'U',
                    'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
                    'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
                    'Ь' => "'",   'Ы' => 'Y',   'Ъ' => "'",
                    'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
                );
                return strtr($string, $converter);
            }

            //Директории для загрузки
            $path = "users/{$_COOKIE['username']}/"; //Папка, куда будут загружаться изображения

              if (!is_dir($path)) //Проверить существует ли папка пользователя , если нет то...
              {
                mkdir($path); //Создать папку
              }
             
            if (isset($_FILES)) //Проверка что выбран файл
            {
            //пролистываем весь массив изображений по одному $_FILES['photo_file']['name'] as $k=>$v
            foreach ($_FILES['photo_file']['name'] as $k=>$v) //k-щечик , v-елемент
                    {
                    if(preg_match('/[.](jpg)|(JPG)|(jpeg)|(JPEG)|(bmp)|(BMP)|(png)|(PNG)$/', $_FILES['photo_file']['name'][$k])) //Верхний и нижний регистр расширений
                    //Ставим допустимые форматы изображений для загрузки (Регулярное выражение)
                        {
                            
                            $filename = $_FILES['photo_file']['name'][$k]; //Оригинальное название файла

                            $replace = str_replace(array('-', '—', '(', ')', ' '), '_', trim($filename)); //Заменить пробели в названии на _
                            $translit = translit($replace); //Транслитерация строки , если есть руские буквы
                            $uploadfile = $translit; //Транслитированая и замененная строка
                            
                            //Замена расширения JPG / png / bmp -> .jpg
                            $uploadfile = str_replace(array('JPG', 'JPEG', 'jpeg', 'PNG', 'png', 'BMP', 'bmp'), 'jpg', trim($uploadfile)); //Новое название файла
                            $source = $_FILES['photo_file']['tmp_name'][$k]; //Временый файл
                            $target = $path . $uploadfile; //Изображения
                            move_uploaded_file($source, $target); //Перемешение изображения в папку 
                        }
                        else
                        {
                        //Если ошибка
                        //echo "ERROR";
                        }
                    }
            //Если все успешно
            //echo "OK";
            }
            header("Location: /?images");
        }
        $this->out('images.php');
    }

    function login(){
        $error="<strong>Ошибка!</strong><br> Неправильный логин или пароль.";

        if(!empty($_POST)){
            $user = $this->db->query("SELECT * FROM users WHERE username = ? AND password = ?",$_POST['login'],$_POST['pass'])->assoc();
            $admin = $this->db->query("SELECT * FROM admin WHERE login = ? AND pass = ?",$_POST['login'],md5($_POST['pass']))->assoc();

            $this->error = $error;
            if ($user) {
                //Пользователь
                $key = md5(microtime().rand(0, 10000));
                setcookie('uid', $user['id'], time()+86400*30, '/');
                setcookie('key', $key, time()+86400*30, '/');
                setcookie('name', $user['name'], time()+86400*30, '/');
                setcookie('username', $user['username'], time()+86400*30, '/');
                $this->db->query("UPDATE users SET cookie = ? WHERE id = ?",$key,$user['id']);
                $_SESSION['user'] = true;
                header("Location: /?baners");
            }

            if ($admin) {
                //Администратор
                $key = md5(microtime().rand(0, 10000));
                setcookie('uid', $admin['id'], time()+86400*30, '/');
                setcookie('key', $key, time()+86400*30, '/');
                setcookie('name', $admin['name'], time()+86400*30, '/');
                setcookie('username', $admin['login'], time()+86400*30, '/');
                $this->db->query("UPDATE admin SET cookie = ? WHERE id = ?",$key,$admin['id']);
                $_SESSION['admin'] = true;
                header("Location: /?baners");
            }
            
        }
        
        $this->out('login.php');
    }
    
    function logoff () {
        setcookie('uid', '', 0, '/');
        setcookie('key', '', 0, '/');
        setcookie('name', '', 0, '/');
        setcookie('admin', '', 0, '/');
        setcookie('username', '', 0, '/');
        session_unset(); //Очистить переменые сессии
        return header("Location: /");
    }
            
    function add() {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        if (!empty($_POST['title'])) {
            $data_start = date("y-m-d");
            $data_end = date("y-m-d");
            if(empty($_POST['url'])){$url=$_SERVER['SERVER_NAME'];} else {$url=$_POST['url'];}

            $this->db->query("INSERT INTO baners (user,title,baner,url,data_start,data_end,enable) VALUES(?,?,?,?,?,?,'false')",$_COOKIE['username'],htmlspecialchars($_POST['title']),$_POST['content'],$url,$data_start,$data_end);
            header("Location: /?baners");
        }
        
        $this->out('add.php');
    }
    
    function del ($id) {
        //echo $id;
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM baners WHERE id = ?",$id);
        header("Location: /?baners");
    }
    
    function edit ($id) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        if (!empty($_POST)){
            $this->db->query("UPDATE baners SET title = ?, url = ?, baner = ? WHERE id = ?",  htmlspecialchars($_POST['title']),$_POST['url'],$_POST['content'],$id);
            return header("Location: /?baners");
        }
        $this->post = $this->db->query("SELECT * FROM baners WHERE id = ?",$id)->assoc();
        $this->out('add.php');
    }


}