-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 02 2015 г., 23:28
-- Версия сервера: 5.5.35-log
-- Версия PHP: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `template_monster`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `cookie` varchar(32) NOT NULL,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `login`, `pass`, `cookie`, `name`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'a399e2c2c61fff568249878ac53184bb', 'Администратор');

-- --------------------------------------------------------

--
-- Структура таблицы `baners`
--

CREATE TABLE IF NOT EXISTS `baners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(60) NOT NULL,
  `title` varchar(80) NOT NULL,
  `baner` mediumtext NOT NULL,
  `url` varchar(60) NOT NULL,
  `enable` varchar(10) NOT NULL,
  `data_start` date NOT NULL,
  `data_end` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `baners`
--

INSERT INTO `baners` (`id`, `user`, `title`, `baner`, `url`, `enable`, `data_start`, `data_end`) VALUES
(1, 'admin', 'Банер №1', 'Вывод текста с форматированием <b>жирный</b>, <i>италик </i>, <u>подчеркнутый</u>, <strike>зачеркнутый </strike>, <b><span style="color: orange;">оранжевый</span>, <span style="color: red;">красный</span>, <span style="color: lime;">зеленый</span>, <span style="color: blue;">синий </span>, <span style="color: magenta;">магента</span>.</b><br>\r\n', 'tm-baners', 'true', '2015-04-01', '2015-04-01'),
(2, 'adming', 'After Effects', '<img width=''100%'' src=''users/admin/After_Effects_CC_2014.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-01'),
(3, 'admin', 'Bridge 1', '<img width=''100%'' src=''users/admin/Bridge_1.jpg''>', 'tm-baners', 'false', '2015-04-01', '2015-04-02'),
(4, 'admin', 'Bridge 2', '<img width=''100%'' src=''users/admin/Bridge_2.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-02'),
(5, 'admin', 'Drift Police', '<img width=''100%'' src=''users/admin/Need_For_Speed_Hot_Pursuit.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-02'),
(6, 'admin', 'BMW', '<img width=''100%'' src=''users/admin/NFS_2012.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-01'),
(7, 'admin', 'BMW M3 GTR', '<img width=''100%'' src=''users/admin/NFS_BMW_M3.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-01'),
(8, 'admin', 'Банер №8', '<img width=''100%'' src=''users/admin/Color_Lines.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-02'),
(9, 'admin', 'Банер №9', '<img width=''100%'' src=''users/admin/Rainbow.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-01'),
(10, 'admin', 'Банер №10', '<img width=''100%'' src=''users/admin/Wallpaper_MoST__White_.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-01'),
(11, 'admin', 'Банер №11', '<img width=''100%'' src=''users/admin/Windows_7_Logon__After_Effects_.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-01'),
(12, 'admin', 'Банер №12', '<img width=''100%'' src=''users/admin/Windows_7__After_Effects__2.jpg''>', 'tm-baners', 'true', '2015-04-01', '2015-04-02');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `baner_id` int(6) NOT NULL,
  `page` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `baner_id`, `page`) VALUES
(1, 1, '*'),
(3, 2, 'index.html'),
(5, 3, 'index.html'),
(6, 3, 'test.php'),
(9, 6, 'index.html'),
(10, 4, 'index.html'),
(11, 6, 'test.html'),
(12, 4, 'test.html'),
(13, 3, 'test.html'),
(14, 7, 'test.html'),
(15, 5, 'test.html'),
(16, 8, 'test.html'),
(17, 9, 'test.html'),
(18, 10, 'test.html'),
(19, 11, 'test.html'),
(20, 23, 'serega.html'),
(21, 12, 'test.html');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `cookie` varchar(32) NOT NULL,
  `key_user` varchar(33) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `cookie`, `key_user`) VALUES
(1, 'user', 'user', 'Serega', '5a17a036e0c8a347b1e4bd762d223c6a', '2430ee6c28739a65728e691d1136fcf8');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
